using UnityEngine;
using System.Collections;

public class NF_Constants{
	// Use this for initialization
		public static string _2DShapesPath= "JW_NamingFruits/Sprites/Shapes/";
		public static string _ColorsPath = "JW_NamingFruits/Sprites/Balloons/";
		public static string _ColorsRefPath = "JW_NamingFruits/Sprites/Balloons/";
		public static string _backgroundsPath= "JW_NamingFruits/Sprites/Backgrounds/";
		public static string _fruitsPath= "JW_NamingFruits/Sprites/Fruits/PNGs/";
		public static string _fruitsTextPath= "JW_NamingFruits/Sprites/FruitName_Texts/PNGs/";
		public static string _alphabetsPath = "JW_NamingFruits/Sprites/Alphabets/";
		public static string _ropePrefabsPath = "JW_NamingFruits/Prefabs/Rope/_newParentForBrokenRope";
		public static string _ropeCutUpPath = "JW_NamingFruits/Prefabs/Rope/ropeCutUp";
		public static string _fruitVOPath ="JW_NamingFruits/Sounds/IN_GAME/";
		public static string _lettersVOPath ="JW_NamingFruits/Sounds/LETTER_NAMES/";
		public static string _questionMark ="JW_NamingFruits/Sprites/Numbers/NumberQuestion";
		public static string _soundsPath ="JW_NamingFruits/Sounds/";
		public static string _balloonVOPath = "JW_NamingFruits/Sounds/COLORS/";
		public static string _colorsVO = "JW_NamingFruits/Sounds/COLORS/BASIC_COLORS/";
		public static string _shapesVO = "JW_NamingFruits/Sounds/SHAPES/BASIC_SHAPES/";
}
