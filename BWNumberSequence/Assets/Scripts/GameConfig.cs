using UnityEngine;
using System.Collections;

public enum AGGameIndex{
	k_None=-1,
	k_NamingFruits=0,
	k_ShapePond,
	k_Beeworld_NumberSequence,
	k_Beeworld_CoutingUpDown,
	k_MemoryAnimal,
	k_2DShapesColors,
	k_FishWorld_HideNSeek
}


public enum AGWorldIndex{
	k_None=-1,
	k_JungleWorld=0,
	k_BeeWorld,
	k_FishWorld,
	k_ShapeWorld,
	k_IckyWorld,
	k_CircusWorld,
	k_VideoWorld
}
